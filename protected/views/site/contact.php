<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */
	$this->pageTitle=Yii::app()->name . 'Contact';
?>

<h1>Contact Me</h1>
<p>If you have any questions, please send me a message using the form below.</p>
<br>
<div class="form">
	<?php
		//Contact form
		$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',
			array(
				'id'=>'user-form',
				'type'=>'vertical',
			)
		);
	?>
	<p class="help-block">Fields with <span class="required">*</span> are required.</p>
	<?php
		echo $form->errorSummary($model);
		echo $form->textFieldRow($model,'firstname',array('class'=>'span5','maxlength'=>45));
		echo $form->textFieldRow($model,'lastname',array('class'=>'span5','maxlength'=>45));
		echo $form->textFieldRow($model,'company',array('class'=>'span5','maxlength'=>45));
		echo $form->textFieldRow($model,'email',array('class'=>'span5','maxlength'=>255));
		echo $form->textFieldRow($model,'subject',array('class'=>'span5', 'maxlength'=>50));
		echo $form->textAreaRow($model,'body',array('class'=>'span5', 'rows'=>10));
	?>
	<div class="form-actions">
		<?php
			//Submit button
			$this->widget('bootstrap.widgets.TbButton',
				array(
					'buttonType'=>'submit',
					'type'=>'primary',
					'label'=>'Send Message',
				)
			);
		?>
	</div>
	<?php
		$this->endWidget();
	?>
</div>


